## basic setup of the windows
#       1. java 
#       2. wix
#       3. maven
#       4. Gitbash

## setting up the environment paths for all
#===========================================start of script========================================

echo "basic setup 1.10==============================================="
'dir env:'
echo "$env : ${CI_PROJECT_DIR}"
echo "$CI_PROJECT_DIR"
echo "$CI_JOB_ID"
echo "$CI_PROJECT_PATH"
echo "$CI_JOB_IMAGE"
echo "$CI_BUILDS_DIR"
echo "$CI_REPOSITORY_URL"
echo "$CI_PROJECT_URL"
echo %CI_JOB_ID%
echo "%CI_JOB_ID%"

#Write-Host "Installing maven ..." -ForegroundColor Cyan /nowait
#    choco install maven -y 
#    $Env:path += ";C:\ProgramData\chocolatey\lib\maven\apache-maven-3.8.6\bin"
#    refreshenv
#   mvn --version
    


## installing java
    Write-Host "Installing OpenJDK ..." -ForegroundColor red 
	$url1 = "https://download.java.net/java/GA/jdk18.0.1.1/65ae32619e2f40f3a9af3af1851d6e19/2/GPL/openjdk-18.0.1.1_windows-x64_bin.zip"
    mkdir C:\temp\OpenJDK
	# Create temp destination for the zip 
	$zipFile1 = "c:\temp\OpenJDK\openjdk.zip" 
	# Download file
     (New-Object Net.WebClient).DownloadFile($url1, $zipFile1) 
    #extracting 
    Expand-Archive -Path C:\temp\OpenJDK\openjdk.zip -DestinationPath "C:\Program Files\Java"
    # adding env var path 
    $Env:path += ";C:\Program Files\Java\jdk-18.0.1.1\bin"
    # checking java installation 
    java --version
# working




## installing wix toolset
Write-Host "Installing WIX toolset ..." -ForegroundColor Cyan
	$url2 = "https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311-binaries.zip"
    mkdir C:\temp\wix
	# Create temp destination for the zip 
	$zipFile2 = "c:\temp\wix\wix.zip" 
	# Download file
     (New-Object Net.WebClient).DownloadFile($url2, $zipFile2) 
    #extracting 
    Expand-Archive -Path C:\temp\wix\wix.zip -DestinationPath "C:\Program Files\wix"
    # adding env var path 
    $Env:path += ";C:\Program Files\wix"
    # checking wix installation 
    candle --version
# working




## installing Gitbash
 Write-Host "Installing Gitbash ..." -ForegroundColor Cyan 
    choco install git -y
    $Env:path += ";C:\Program Files\Git\bin"
    refreshenv
    # checking Git installtion
    git --version
    bash --version

# working   



## outputing the env variables path
    Write-Host "env var path ..." -ForegroundColor Cyan
    $env:path.Split(";")



## getting the ipinfo
    #Write-Host "IP INFO ..." -ForegroundColor Cyan
    #Invoke-RestMethod -Uri https://ipinfo.io


# running maven build commands
    #cd C:/builds/project-0/
    #$Env:MAVEN_OPTS="-Xmx3000m"
    #mvn -B clean install
    #mvn -B -P compressXZ -f weasis-distributions clean package 
#working
    $url = "https://gitlab.com/deepakbhandari1997/weas/-/jobs/$CI_JOB_ID/artifacts/download?file_type=archive"
	# Create temp destination for the zip and get zipfile name from source URL
	$zipFile = "$CI_PROJECT_DIR\weasis-distribution\target\portable-dist\weasis-portable.zip" #+ $(Split-Path -Path $Url -Leaf)
    Measure-Command { (New-Object Net.WebClient).DownloadFile($url, $zipFile) } -Verbose    

    echo $zipFile

    dir "weasis-distributions\target\"
    echo "starting the archive extraction"
	Expand-Archive -Path "weasis-distributions\target\portable-dist\weasis-portable.zip" -DestinationPath "weasis-distributions\target\portable-dist\weasis-portable" -verbose
    dir "weasis-distributions\target\portable-dist\weasis-portable"
    dir "weasis-distributions\target\portable-dist\weasis-portable\weasis"
    


    echo "starting the bash script"
    cd "weasis-distributions\script"
    bash package-weasis.sh -i "weasis-distributions\target\portable-dist\weasis-portable" -o "weasis-distributions\target\output" -j "C:\Program Files\Java\jdk-18.0.1.1"















